/*! lu-svg - 2017-03-17 */
angular.module('lu-svg', [])
    .directive('luSvg', LuSvgDetails);

LuSvgDetails.$inject = ['$log'];
function LuSvgDetails($log) {
    function controller() {
        var vm = this;
        vm.class;
        vm.svgId = '#';

        vm.$onInit = function() {
            if (angular.isDefined(vm.asString) && angular.isString(vm.asString) && angular.isUndefined(vm.luSvg)) {
                updateSvg(vm.asString);
            } else if (angular.isDefined(vm.luSvg)) {
                updateSvg(vm.luSvg);
            } else {
                $log.error('A svg identifier is required');
            }
        };

        function updateSvg(string) {
            vm.class = string;
            vm.svgId = vm.svgId.concat(vm.class);
        }
    }

    var config = {
        restrict: 'A',
        scope: true,
        controller: controller,
        controllerAs: 'vm',
        bindToController: {
            luSvg: '=?',
            asString: '@'
        },
        template: '<svg class="icon {{vm.class}}"><use xlink:href="{{vm.svgId}}"></use></svg>'
    };

    return config;
}
