"use strict";

angular.module("menu", ["pageManager"])

	// controller for the view portion - see example view for proper instantiation
	.controller("MenuController", ["$scope", "$location", "$filter", "pages", function ($scope, $location, $filter, pages) {

		$scope.menuItems = pages;

		$scope.isCurrentPage = function (menuItem) {
			return $filter("pathToLink")($location.path()) === $filter("pathToLink")(menuItem.path);
		};

	}])

	// filter used internally to match the current page to a path string.
	.filter("pathToLink", function () {
		return function (input) {
			var slash = input.substring(1).indexOf("/");
			return input.substring(0, slash === -1 ? undefined : slash + 2) + (slash === -1 ? "/" : "");
		};
	});
