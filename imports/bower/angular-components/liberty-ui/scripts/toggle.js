/*
    
    Toggle directive
    @author Matthias Dailey
    @version 1.0.0
    
    @plunk http://plnkr.co/edit/fjSTwp?p=info

## Toggle

Toggle switch handle is not draggable. Just click or tap the thing and it switches.

# Usage:
    <toggle ng-model="student.alive"></toggle>

# Styles
Styles are external to this directive. See included CSS file.

*/

"use strict";

// namespace and cautious loading
var lu = lu || {};
lu.ngLuStyle = lu.ngLuStyle || angular.module('luStyle', []);

lu.ngLuStyle.directive('toggle', ["$log", function($log){
    "use strict";
    return {
        restrict: 'E',
        require: '^ngModel',
        template: ''
            + '<div class="toggle on">'
                + '<span class="handle"></span>'
                + '<span class="bk-on">ON</span>'
                + '<span class="bk-off">OFF</span>'
            + '</div>'
        ,
        link: function (scope, element, attrs, ngModel) {
            
            var originalModel = null;
            var model = false;
            
            // elements
            var $toggle = element.find('div');
            var $handle = element.find('span');
            
            // if there's no ngModel
            if (!attrs.ngModel) {
                // no ngModel attached
                $log.warn('No model attached');
                return;
            }
            
            // watch for changes to model
            scope.$watch(attrs['ngModel'], function(newVal){
               var boolVal = newVal;
               if (attrs.boolModel === "") {
                  if (attrs.boolModelTrue === newVal) {
                     boolVal = true;
                  } else {
                     boolVal = false;
                  }
               }
               toggle(boolVal);
               model = !!boolVal;
                // save initial 'true' value
                if (newVal && originalModel===null) {
                    originalModel = newVal
                }
            });
            
            element.on('click', function(e){
                // set (toggle) the model value
                // if the model was truthy
                if (model) {
                    model = false;
                }
                else {
                    if (originalModel) {
                        // set the model to the orignial value
                        model = originalModel;
                    }
                    else {
                        model = true;
                    }
                }
                // update the ngModel
                ngModel.$setViewValue(model);
                //ngModel.$modelValue = model;
                scope.$apply();
                e.preventDefault();
            });
            
            var toggle = function (tf) {
                tf ?  $toggle.addClass('on') : $toggle.removeClass('on');
            }
            
        }
    };
}]) // end directive luStyle.toggle

