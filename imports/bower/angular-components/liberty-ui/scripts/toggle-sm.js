/*
    
    Small Toggle directive
    @author Matthias Dailey
    @version 1.0.0
    
    @plunk http://plnkr.co/edit/fjSTwp?p=info



Toggle switch handle is not draggable. Just click or tap the thing and it switches.

# Usage:
    <toggle-sm ng-model="student.alive"></toggle-sm>

# Styles
Styles are external to this directive. Example styles:

    toggle-sm .toggle {
        width: 2.25em; height: 1em;
        border-radius: 1em;
        background: #d7d7d7;
        box-shadow:
            inset 0 .0625em .125em rgba(0,0,0,.15),
            0 .0625em 0 rgba(255,255,255,.65)
            ;
        position: relative;
        transition: all .12s;
        cursor: pointer;
    }
    toggle-sm .toggle.on {
        background: #324b6f;
    }
    toggle-sm .handle {
        width: .875em;
        border-radius: 1em;
        box-shadow: 
            inset 0 .0625em .0625em #fff,
            0 .0625em 0 rgba(0,0,0,.15)
            ;
        background-color: #f9f9f9;
        background-image: -webkit-gradient(linear, left top, left bottom, from(#f9f9f9), to(#e9e9e9));
        background-image: -webkit-linear-gradient(top, #f9f9f9, #e9e9e9);
        background-image:    -moz-linear-gradient(top, #f9f9f9, #e9e9e9);
        background-image:      -o-linear-gradient(top, #f9f9f9, #e9e9e9);
        background-image:         linear-gradient(to bottom, #f9f9f9, #e9e9e9);
        position: absolute;
        top: .0625em;
        bottom: .0625em;
        left: .0625em;
        transition: all .12s;
    }
    toggle-sm .on .handle {
        left: 1.3125em;
    }


*/

"use strict";

// namespace and cautious loading
var lu = lu || {};
lu.ngLuStyle = lu.ngLuStyle || angular.module('luStyle', []);

lu.ngLuStyle.directive('toggleSm', ["$log", function($log){
    "use strict";
    return {
        restrict: 'E',
        require: '^ngModel',
        template: ''
            + '<div class="toggle">'
                + '<span class="handle"></span>'
            + '</div>'
        ,
        link: function (scope, element, attrs, ngModel) {
            
            var originalModel = null;
            var model = false;
            
            // elements
            var $toggle = element.find('div');
            var $handle = element.find('span');
            
            // if there's no ngModel
            if (!attrs.ngModel) {
                // no ngModel attached
                $log.warn('No model attached');
                return;
            }
            
            // watch for changes to model
            scope.$watch(attrs['ngModel'], function(newVal){
               var boolVal = newVal;
               if (attrs.boolModel === "") {
                  if (attrs.boolModelTrue === newVal) {
                     boolVal = true;
                  } else {
                     boolVal = false;
                  }
               }
               toggle(boolVal);
               model = !!boolVal;
                // save initial 'true' value
                if (newVal && originalModel===null) {
                    originalModel = newVal
                }
            });
            
            element.on('click', function(e){
                // set (toggle) the model value
                // if the model was truthy
                if (model) {
                    model = false;
                }
                else {
                    if (originalModel) {
                        // set the model to the orignial value
                        model = originalModel;
                    }
                    else {
                        model = true;
                    }
                }
                // update the ngModel
                ngModel.$setViewValue(model);
                //ngModel.$modelValue = model;
                scope.$apply();
                e.preventDefault();
            });
            
            var toggle = function (tf) {
                tf ?  $toggle.addClass('on') : $toggle.removeClass('on');
            }
            
        }
    };
}]) // end directive luStyle.toggleSm

