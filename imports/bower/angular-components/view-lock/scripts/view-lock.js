"use strict";

angular.module("viewLock", [])

	.factory("ViewLock", ["$rootScope", function ($rootScope) {

		var locationListener;

		var ViewLock = {

			/**
			 * Bind lock()/unlock() to a boolean $rootScope variable.
			 * @param  {string} variable the name of the $rootScope variable to bind to
			 * @return {null}
			 */
			bind: function (variable) {

				$rootScope.$watch(variable, function () {
					$rootScope[variable] ? ViewLock.lock() : ViewLock.unlock();
				});

			},

			/**
			 * Lock the view (prevent changing views or leaving the page).
			 * @param  {string} message the message to be displayed in confirm()
			 * @return {null}
			 */
			lock: function (message) {
				
				$(window).on("beforeunload", function () {
					return message || "Your changes have not yet been saved. Continue?";
				});

				locationListener = $rootScope.$on("$locationChangeStart", function (ngEvent) {
					if (!window.confirm(message || "Your changes have not yet been saved. Continue?")) {
						ngEvent.preventDefault();
					}
				});

			},

			/**
			 * Unlock the view.
			 * @return {null}
			 */
			unlock: function () {
				$(window).off("beforeunload");
				if (locationListener) locationListener(); // kill listener
			}
		
		};

		return ViewLock;

	}]);
