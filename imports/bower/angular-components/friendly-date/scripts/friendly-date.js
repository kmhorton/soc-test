"use strict";

angular.module("friendlyDate", ['luDate'])

/*
 * 
 */
    .filter("friendlyDate", ["$filter", function($filter) {
        //TODO: make the formats customizable
        var formats = {
            today: "Today",
            tomorrow: "Tomorrow",
            weekdayDay: "EEE doo",
            monthDay: "MMM doo",
            monthDayYear: "MMM doo, yyyy"
        };

        var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds

        function treatAsUTC(date) {
            var result = new Date(date);
            result.setMinutes(result.getMinutes() - result.getTimezoneOffset());
            return result;
        };

        function daysBetween(startDate, endDate) {
            return Math.floor(treatAsUTC(endDate) / oneDay) - Math.floor(treatAsUTC(startDate) / oneDay);
        };

        return function(date) {
            if (angular.isString(date) || angular.isNumber(date)) {
                date = new Date(date);
            }

            if (!angular.isDate(date) || !isFinite(date.getTime())) {
                return date;
            }
            
            var now = new Date();
            var diff = daysBetween(now, date);

            //TODO: make the rules customizable
            if (diff < 0) { //in the past
                return $filter('luDate')(date, formats.monthDayYear);
            } else if (diff == 0) { //today
                return formats.today;
            } else if (diff == 1) { //tomorrow
                return formats.tomorrow;
            } else if (diff < 31 && date.getMonth() == now.getMonth()) { //same month
                return $filter('luDate')(date, formats.weekdayDay);
            } else if (date.getFullYear() == now.getFullYear()) { //same year
                return $filter('luDate')(date, formats.monthDay);
            } else { //different year
                return $filter('luDate')(date, formats.monthDayYear);
            }
        };
    }]);