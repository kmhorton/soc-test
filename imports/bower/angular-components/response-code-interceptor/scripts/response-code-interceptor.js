"use strict";

angular.module("responseCodeInterceptor", [])
	.provider("ResponseCodeInterceptor", ["$httpProvider", function ($httpProvider) {

		return {

			/**
			 * Configures and enables a new interceptor for a given HTTP response code (status)
			 * @param  {integer} status      the status code to set the interceptor for
			 * @param  {function} interceptor a function that returns a promise or response value, to be injected in the response promise chain for any response matching the given response code
			 * @return {null}             
			 */
			setInterceptor: function (status, interceptor) {
				$httpProvider.interceptors.push(["$q", "$injector", function ($q, $injector) {

					if (status >= 200 && status <= 299) {
						return {
							response: function (response) {
								return response.status === status ? interceptor(response, $injector) : response || $q.when(response, $injector);
							}
						};
					} else {
						return {
							responseError: function (response) {
								return response.status === status ? interceptor(response, $injector) : $q.reject(response, $injector);
							}
						};
					}

				}]);
			},
			
			/**
			 * Blank getter function - this provider does not provide anything.
			 * @return {null}
			 */
			$get: function () {

			}

		};
	}]);