"use strict";

angular.module("pageManager", [])

	// provider for managing pages/views
	.provider("pages", ["$routeProvider", function ($routeProvider) {

		var pages = [],
			locked = false;

		return {

			/**
			 * Add a page to the page array:
			 * @param  {Object} page a page object:
			 * {
			 *   path: "/some-path-string",
			 *   controller: "SomeController",
			 *   templateUrl: "views/some-view.html",
			 *   text: "Text of the Menu Item", // this and "icon" below are for the optional menu module
			 *   icon: "A" // icon of the menu item
			 * }
			 * @return {null}
			 */
			addPage: function (page) {
				if (locked) {
					throw "addPage: routes have already been set up";
				}

				pages.push(page);
			},

			/**
			 * Add an array of pages to the page array
			 * @param  {Array<Object>} pageArray an array of page objects
			 * @return {null}
			 */
			addPages: function (pageArray) {
				if (locked) {
					throw "addPages: routes have already been set up";
				}

				pages = pages.concat(pageArray);
			},

			/**
			 * Get current pages array
			 * @return {Array<Object>} pages
			 */
			getPages: function () {
				return pages;
			},

			/**
			 * Set up $routeProvider to route according to the pages array
			 * @return {null}
			 */
			setupRoutes: function () {

				if (!pages || !pages.length) {
					throw "setupRoutes: no pages have been added";
				}

				if (locked) {
					throw "setupRoutes: routes have already been set up";
				}

				angular.forEach(pages, function (page) {
					$routeProvider.when(page.path, page);
				});

				$routeProvider.otherwise({
					redirectTo: pages[0].path
				});

				locked = true;

			},

			// provider instantiation
			$get: function () {
				return pages;
			}

		};

	}])

	.directive("page", ["$location", "$rootScope", function ($location, $rootScope) {

		return function (scope, element, attrs) {
			scope.$on("$routeChangeSuccess", function () {
				element.attr("page", $location.path().match(/^\/([^/]*)/)[1]);
			});
		};

	}]);
