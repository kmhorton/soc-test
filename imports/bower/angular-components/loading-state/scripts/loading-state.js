"use strict";

angular.module("loadingState", [])
	
	.directive("loading", ["$timeout", "LoadingStateManager", function ($timeout, LoadingStateManager) {

		// check for spin.js
		if (!("Spinner" in window)) {
			throw "loadingState module: spin.js not detected";
		}

		return function (scope, element, attrs) {
			var spinner,
				doneCountdown,
				timer,
				currentState = false;

			scope.$on("loadingStateChange", function () {

				var loadingState = LoadingStateManager.getLoading(attrs.loading);

				if (loadingState !== currentState) {

					currentState = loadingState;

					if (loadingState === true && !spinner) {
						
						if (doneCountdown) {
							$timeout.cancel(doneCountdown);
							doneCountdown = false;
						}

						element.addClass("loading");

						spinner = new Spinner({
							lines: 11, // The number of lines to draw
							length: 5, // The length of each line
							width: 3, // The line thickness
							radius: 7, // The radius of the inner circle
							corners: 1, // Corner roundness (0..1)
							rotate: 0, // The rotation offset
							color: '#fff', // #rgb or #rrggbb
							speed: 1, // Rounds per second
							trail: 60, // Afterglow percentage
							shadow: false, // Whether to render a shadow
							hwaccel: false, // Whether to use hardware acceleration
							className: 'spinner', // The CSS class to assign to the spinner
							zIndex: 2e9, // The z-index (defaults to 2000000000)
							top: 'auto', // Top position relative to parent in px
							left: 'auto' // Left position relative to parent in px
						}).spin();

						element.append(spinner.el);

						if (timer) $timeout.cancel(timer);
						timer = $timeout(function () {}, 300);

					} else if (loadingState === false && spinner) {

						var stopLoading = function () {
							if (spinner && spinner.stop) spinner.stop();
							spinner = null;
							element.removeClass("done").removeClass("loading");
						};

						if (timer) {
							timer.then(function () {
								stopLoading();
							});
						} else {
							stopLoading();
						}

					} else if (loadingState === "done") {

						if (spinner && spinner.stop) spinner.stop();
						if (spinner) spinner = null;

						element.addClass("done");

						doneCountdown = $timeout(function () {
							element.removeClass("done").removeClass("loading");
						}, 1000);

					}

				}
				
			});
		};

	}])
	
	.factory("LoadingStateManager", ["$rootScope", function ($rootScope) {

		var loadingStates = {};

		return {
			setLoading: function (id, state) {
				loadingStates[id] = state;
				$rootScope.$broadcast("loadingStateChange");
			},
			getLoading: function (id) {
				return loadingStates[id] || false;
			}
		};

	}]);
