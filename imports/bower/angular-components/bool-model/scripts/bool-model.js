/* 
 * Directive to convert the type of data saved by ng-model for booleans.
 * This enables two-way binding of a boolean input to a string variable
 * 
 * Extended from the solution found here:
 * http://stackoverflow.com/a/12947995/3123195
 */
"use strict";

angular.module("boolModel", [])

    .directive("boolModel", function () {
        return {
            require: 'ngModel',
            link: function(scope, element, attr, ngModelCtrl) {
                ngModelCtrl.$parsers.push(function viewToModel(input) {
                    return input ? attr.boolModelTrue : attr.boolModelFalse;
                });

                ngModelCtrl.$formatters.push(function modelToView(input) {
                    return input == attr.boolModelTrue;
                });
            }
        };
    });