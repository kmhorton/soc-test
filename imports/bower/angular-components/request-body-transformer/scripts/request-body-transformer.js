"use strict";

angular.module("requestBodyTransformer", [])
	.provider("RequestBodyTransformer", function () {

		return {

			/**
			 * Initialize and enable the request transformer
			 * @param  {Provider} $httpProvider the $httpProvider object from Angular
			 * @return {null}
			 */
			setup: function ($httpProvider) {
				
				$httpProvider.defaults.transformRequest.push(function (data, headersGetter) {
					if (data && data.charAt(0) === "{") {
						var obj = angular.fromJson(data);

						// extract the requestBody as a string if present
						if (typeof obj.requestBody !== "undefined") {
							return typeof obj.requestBody === "object" ? angular.toJson(obj.requestBody) : obj.requestBody.toString();
						} else {
							return data;
						}
					} else {
						return data;
					}
				});
				
			},

			$get: function () {
				
			}

		};

	});