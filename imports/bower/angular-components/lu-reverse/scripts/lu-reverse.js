"use strict";

angular.module("luReverse", [])

    /**
     * Filter to reverse the order of an array (much faster than Array.reverse() )
     * 
     * inspired by: http://stackoverflow.com/q/5276953/3123195
     * copied from here: http://jsperf.com/js-array-reverse-vs-while-loop/57
     */
    .filter('luReverse', function() {
    return function reverse(array) {
        var left = null;
        var right = null;
        for (left = 0; left < array.length / 2; left += 1) {
            right = array.length - 1 - left;
            var temporary = array[left];
            array[left] = array[right];
            array[right] = temporary;
        }

        return array;
    };
});