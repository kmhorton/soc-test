"use strict";

angular.module("nestedSortable", [])
    
    /*
     * Nested Sortable Directive
     * 
     * This is a tailored version of the general-purpose directive found here: https://github.com/thgreasi/tg-dynamic-directive/blob/master/src/tg.dynamic.directive.js
     * 
     * Usage:
     * 
     * <nested-sortable ng-model="hierarchy" nested-sortable-view="nestable_sortable.tpl.html"></nested-sortable>
     * 
     * ng-model: a nested data structure
     * nested-sortable-view: url to a template html file which includes a nested-sortable element (or not). For example:
     * 
     *  {{ngModelItem.description}}
     *  <ul class="sortable-container" ng-class="{empty: !ngModelItem.titles.length}" ui-sortable="{connectWith: '.sortable-container'}" ng-model="ngModelItem.titles">
     *      <li ng-repeat="innerItem in ngModelItem.titles">
     *          <nested-sortable ng-model="innerItem" nested-sortable-view="nestable_sortable.tpl.html">
     *          </nested-sortable>
     *      </li>
     *  </ul>
     * 
     */
    .directive("nestedSortable", ["$compile", function($compile) {
        return {
            restrict: "AE",
            require: "^ngModel",
            scope: true,
            template: "<ng-include src='templateUrl'></ng-include>",
            link: function(scope, element, attrs, ngModel) {
                //TODO: perhaps this can be streamlined a little more...
                scope.ngModelItem = scope.$eval(attrs.ngModel);
                scope.templateUrl = (scope.ngModelItem == null) ? null : attrs.nestedSortableView;
            }
        };
    }]);