"use strict";

angular.module("maskedState", [])
	
	.directive("masked", function () {

		return function (scope, element, attrs) {

			attrs.$observe("masked", function (maskedState) {

				if (maskedState === "true") maskedState = true;
				else if (maskedState === "false") maskedState = false;

				if (maskedState) {
					element.toggleClass("masked", true);
					if (element.has($(document.activeElement))) {
						$(document.activeElement).blur();
					}
				} else {
					element.toggleClass("masked", false);
				}

			});

		};

	});
