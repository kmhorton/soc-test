"use strict";

angular.module("luDate", [])

    /**
     * Filter that adds to angular's date filter ordinal numbers and lowercase meridian
     * 
     * oo - replaced with ordinal number [st, nd, rd, th]
     * a - AM/PM (this is default of date filter)
     * A - am/pm lowercase
     */
    .filter("luDate", ["$filter", function($filter) {
        var suffixes = ["th", "st", "nd", "rd"];

        var replaceNotQuoted = function(string, match, replacement) {
            string = string.split("''").join("'ESC_QUOTE'"); // Remove escaped quotes to add back later

            var replacementString = "";
            angular.forEach(string.split("'"), function(substring, index) {
                if (index % 2 == 0) {
                    replacementString += substring.split(match).join(replacement);
                } else {
                    replacementString += "'" + substring + "'";
                }
            });

            replacementString = replacementString.split("''").join(""); // Remove quotes which bunched together and look like escaped quotes
            replacementString = replacementString.split("'ESC_QUOTE'").join("''"); // Add escaped quotes back

            return replacementString;
        };

        return function(input, format) {
            if (input == null) {
                return "";
            }

            format = replaceNotQuoted(format, "A", "'LUMERIDIAN'"); // To avoid collisions with regular A showing up
            format = replaceNotQuoted(format, "oo", "'LUORDINAL'"); // avoid collision with words like "boot"

            var dtfilter = $filter("date")(input, format);

            // Insert Ordinal number
            var day = parseInt($filter("date")(input, "dd"));
            var relevantDigits = (day < 30) ? day % 20 : day % 30;
            var suffix = (relevantDigits <= 3) ? suffixes[relevantDigits] : suffixes[0];
            dtfilter = dtfilter.split("LUORDINAL").join(suffix);

            // Insert lowercase meridian
            var meridian = $filter("date")(input, "a").toLowerCase();
            dtfilter = dtfilter.split("LUMERIDIAN").join(meridian);

            return dtfilter;
        };
    }]); 
