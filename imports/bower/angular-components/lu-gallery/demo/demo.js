/*
 * This file defines an app for demonstration purposes
 */
angular.module('demo', ['luGallery']);

angular.module('demo').controller('myController', function($scope) {
    $scope.collection = [
        {src: 'http://i.imgur.com/KFLZhko.png', thumb: 'http://i.imgur.com/KFLZhko.png', description: 'Bear'},
        {src: 'http://i.imgur.com/SUsIoQT.png', thumb: 'http://i.imgur.com/SUsIoQT.png', description: 'Success kid'},
        {src: 'http://i.imgur.com/fvcAovP.jpg', thumb: 'http://i.imgur.com/fvcAovP.jpg', description: 'Captain Jack'}
    ];
    
    $scope.collection2 = [
        {src: 'http://i.imgur.com/J08m8ai.jpg', thumb: 'http://i.imgur.com/J08m8ai.jpg', description: 'Darth Maul'},
        {src: 'http://i.imgur.com/Q23cRoY.jpg', thumb: 'http://i.imgur.com/Q23cRoY.jpg', description: 'C3P0'},
        {src: 'http://i.imgur.com/BxmfSK7.jpg', thumb: 'http://i.imgur.com/BxmfSK7.jpg', description: 'Bear again'},
        {src: 'http://i.imgur.com/iVcUR18.jpg', thumb: 'http://i.imgur.com/iVcUR18.jpg', description: 'Stay thirsty'},
        {src: 'http://i.imgur.com/vVMQKwC.jpg', thumb: 'http://i.imgur.com/vVMQKwC.jpg', description: 'BoA'},
    ];
});