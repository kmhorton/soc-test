/*
 * This component was forked from ngGallery (https://github.com/jkuri/ngGallery)
 */
angular.module('luGallery', []);

/*
 * luGallery directive
 * 
 * A directive for providing slideshow/gallery functionality. Allows easy linking to a list of thumbnails.
 * 
 * Params:
 *     images - array of objects with the following properties:
 *         src         - url for the full-size image
 *         thumb       - url for the thumbnail image
 *         description - (optional) text to be displayed below the image
 *     thumbsNum - (optional) how many thumbnails should be displayed at the bottom of the gallery simultaneously (min: 3, max: 11, default: array length minus one)
 *     srcProp   - (optional) name of src property on image object (above) (default 'src')
 *     thumbProp - (optional) name of thumb property on image object (above) (default 'thumb')
 *     descProp  - (optional) name of description property on image object (above) (default 'description')
 * 
 * Usage: (see demo for example)
 *     inside of lu-gallery, the following properties are available:
 *         images        - the array of objects passed in (presumably this would be passed into ng-repeat to generate thumbnails)
 *         openGallery() - function which displays the gallery to the image at the given position in the images array
 */
angular.module('luGallery').directive('luGallery', ["$document", "$timeout", "$q", "$templateCache", function($document, $timeout, $q, $templateCache) {
    'use strict';
    
    var thumbsNum,
        keys_codes = {
            enter : 13,
            esc   : 27,
            left  : 37,
            right : 39
        },
        templateUrl = 'lu-gallery.html';

    $templateCache.put(templateUrl,
        '<div class="lu-gallery-overlay" ng-show="opened"></div>' +
        '<div class="lu-gallery-content" ng-show="opened">' +
            '<div class="uil-ring-css" ng-show="loading"><div></div></div>' +
            '<a class="close-popup glyphicon glyphicon-remove" ng-click="closeGallery()"></a>' +
            '<a class="nav-left glyphicon glyphicon-menu-left" ng-click="prevImage()"></a>' +
            '<img ng-src="{{ src }}" ng-click="nextImage()" ng-show="!loading" class="effect"/>' +
            '<a class="nav-right glyphicon glyphicon-menu-right" ng-click="nextImage()"></a>' +
            '<span class="info-text">{{ index + 1 }}/{{ images.length }}<span ng-if="description"> - {{ description }}</span></span>' +
            '<div class="thumbnails-wrapper">' +
                '<div class="thumbnails">' +
                    '<div ng-repeat="i in images">' +
                        '<img ng-src="{{ i[thumbProp] }}" ng-class="{\'active\': index === $index}" ng-click="changeImage($index)"/>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>');
    
    return {
        restrict: 'EA',
        scope: {
            images: "="
        },
        transclude: true,
        templateUrl: templateUrl,
        controller: ["$scope", "$element", "$attrs", function controller($scope, $element, $attrs) {
            var $body = $document.find('body');
            
            var $thumbwrapper = angular.element($element[0].querySelectorAll('.thumbnails-wrapper'));
            var $thumbnails = angular.element($element[0].querySelectorAll('.thumbnails'));
            
            /*
             * thumbsNum should be:
             *  at least 3
             *  no more than 11
             *  no more than the number of images minus one
             *  odd
             */
            thumbsNum = Math.max(3, Math.min(11, $scope.images.length - 1, ($attrs.thumbsNum || 11)));
            if (thumbsNum % 2 == 0) thumbsNum--;

            var calculateThumbsWidth = function () {
                var width = 0,
                    visible_width = 0;
                angular.forEach($thumbnails.find('img'), function(thumb) {
                    width += thumb.clientWidth;
                    width += 10; // margin-right
                    visible_width = thumb.clientWidth + 10;
                });
                return {
                    width: width,
                    visible_width: visible_width * thumbsNum
                };
            };

            var smartScroll = function (index) {
                $timeout(function() {
                    var len = $scope.images.length,
                        width = $scope.thumbs_width,
                        current_scroll = $thumbwrapper[0].scrollLeft,
                        item_scroll = parseInt(width / len, 10),
                        i = index + 1,
                        s = Math.ceil(len / i);

                    $thumbwrapper[0].scrollLeft = 0;
                    $thumbwrapper[0].scrollLeft = i * item_scroll - (s * item_scroll);
                }, 100);
            };

            var loadImage = function (i) {
                var deferred = $q.defer();
                var image = new Image();

                image.onload = function () {
                    $scope.loading = false;
                    if (typeof this.complete === false || this.naturalWidth === 0) {
                        deferred.reject();
                    }
                    deferred.resolve(image);
                };

                image.onerror = function () {
                    deferred.reject();
                };

                image.src = $scope.images[i][$scope.srcProp];
                $scope.loading = true;

                return deferred.promise;
            };

            var showImage = function (i) {
                loadImage($scope.index).then(function(image) {
                    $scope.src = image.src;
                    smartScroll($scope.index);
                });
                $scope.description = $scope.images[i][$scope.descProp] || '';
            };

            angular.extend($scope, {
                index: 0,
                opened: false,

                thumb_wrapper_width: 0,
                thumbs_width: 0,

                srcProp: $attrs.srcProp || 'src',
                thumbProp: $attrs.thumbProp || 'thumb',
                descProp: $attrs.descProp || 'description',

                changeImage: function (i) {
                    $scope.index = i;
                    showImage($scope.index);
                },

                nextImage: function () {
                    $scope.index += 1;
                    if ($scope.index === $scope.images.length) {
                        $scope.index = 0;
                    }
                    showImage($scope.index);
                },

                prevImage: function () {
                    $scope.index -= 1;
                    if ($scope.index < 0) {
                        $scope.index = $scope.images.length - 1;
                    }
                    showImage($scope.index);
                },

                openGallery: function (i) {
                    i = parseInt(i, 10) || 0;
                    
                    $scope.index = i;
                    showImage($scope.index);
                    
                    $scope.opened = true;

                    $timeout(function() {
                        var calculatedWidth = calculateThumbsWidth();
                        $scope.thumbs_width = calculatedWidth.width;
                        $thumbnails.css({ width: calculatedWidth.width + 'px' });
                        $thumbwrapper.css({ width: calculatedWidth.visible_width + 'px' });
                        smartScroll($scope.index);
                    });
                },

                closeGallery: function () {
                    $scope.opened = false;
                }
            });

            //TODO: add swipe support
            $body.bind('keydown', function(event) {
                if (!$scope.opened) {
                    return;
                }
                var which = event.which;
                if (which === keys_codes.esc) {
                    $scope.closeGallery();
                } else if (which === keys_codes.right || which === keys_codes.enter) {
                    $scope.nextImage();
                } else if (which === keys_codes.left) {
                    $scope.prevImage();
                }

                $scope.$apply();
            });
        }],
        link: function postLink(scope, element, attrs, controllerFn, transcludeFn) {
            transcludeFn(scope, function(clone, scope) {
                element.append(clone);
            });
        }
    };
}]);
