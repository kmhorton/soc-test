"use strict";

/**
 * Ordinal filter kind of ripped off of someone on the interwebs
 */
angular.module("ordinal", [])
    
    .filter("ordinal", ["$filter", function($filter) {
        var suffixes = ["th", "st", "nd", "rd"];

        return function(input, format) {
            if (input == null) {
                return "";
            }

            var mod = input % 100;
            var suffix = suffixes[(mod-20)%10]||suffixes[mod]||suffixes[0];

            return input + suffix;
        };
    }]);
