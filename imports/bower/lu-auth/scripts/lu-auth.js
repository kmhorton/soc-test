/*!
 * lu-auth  - 20160831
 * Copyright 2015 Liberty University
 */


'use strict';



// Source: app/scripts/config.js

angular.module("luAuth", ["ngResource", "mgcrea.ngStrap.modal"]);

angular.module("luAuth").config(["$httpProvider", function ($httpProvider) {
    $httpProvider.interceptors.push("AuthenticationInterceptor");
}]);



// Source: app/scripts/authentication-interceptor.js

angular.module("luAuth").factory('AuthenticationInterceptor', ["$q", "$injector", function ($q, $injector) {
    var errorMessageTimer = null;

    var displayErrorModal = function (title, content) {
        var $modal = $injector.get("$modal");
        if (errorMessageTimer == null || (Date.now() - errorMessageTimer.getTime() > 1000)) { // Last error displayed was more than 1 second ago
            errorMessageTimer = new Date();
            $modal({title: title, content: content, show: true, placement: 'center'});
        }
    };

    return {
        'request': function (requestConfig) {
            var AuthenticationService = $injector.get("AuthenticationService");

            var app = AuthenticationService.findMatchingApp(requestConfig.url);

            if (app == null || !app.preemptiveAuthentication) {
                //if no configuration for the given url was found, or if we don't do preemptiveAuthentication against this url, then this isn't a url that we should authenticate against
                return requestConfig;
            }

            //this is a url that we should authenticate against
            var deferred = $q.defer();

            AuthenticationService.ensureAuthentication(app, deferred, requestConfig);

            return deferred.promise;
        },
        'responseError': function (rejection) {
            var AuthenticationService = $injector.get("AuthenticationService");

            switch (rejection.status) {
                case 401: // Unauthorized
                    var app = AuthenticationService.findMatchingApp(rejection.config.url);
                    
                    if (app) AuthenticationService.authenticate(app);

                    break;
                case 403: // Denied - nothing can be done, inform user
                    //if the request was for a roles.json file, don't show the popup
                    if (rejection.config.url.indexOf('roles.json') == -1) {
                        displayErrorModal('Denied', "You do not have access to this resource.");
                    }

                    break;
            }

            //passthrough for all other response errors
            return $q.reject(rejection);
        }

    };
}]);




// Source: app/scripts/authentication-service.js

angular.module("luAuth").provider("AuthenticationService", function () {
    var AuthenticationService = null;

    //configuration items, with default values
    var allowRelativeUrls = false;

    //functions available during application config stage
    angular.extend(this, {
        setAllowRelativeUrls: function(input) { allowRelativeUrls = input; },
        //TODO: we should probably support authentication events as well...
    });

    function isAbsoluteUrl(url) {
        //got the regex from here: http://stackoverflow.com/a/19709846/3123195
        return /^(?:[a-z]+:)?\/\//i.test(url);
    }

    this.$get = ["Urls", "$window", "$http", "$modal", "$q", function(Urls, $window, $http, $modal, $q) {
        //polyfill for window.location.origin
        //gotten from here: http://stackoverflow.com/a/22564291/3123195
        if (!$window.location.origin) {
            $window.location.origin = $window.location.protocol + "//" + $window.location.hostname + ($window.location.port ? ':' + $window.location.port : '');
        }
        
        var authenticating = false;
        var unreachableFlag = {};

        /* SETUP */
        unreachableFlag[Urls.baseUrl] = false;

        angular.forEach(Urls.allOtherAppUrls, function (appUrl) {
            unreachableFlag[appUrl] = false;
        });

        var isExcludedUrl = function(checkUrl) {
            var exclude = false;
            angular.forEach(Urls.excludedUrls, function (excludedUrl) {
                if (checkUrl.indexOf(excludedUrl) > -1) {
                    exclude = true;
                }
            });
            return exclude;
        };

        /* PUBLIC FUNCTIONS */
        AuthenticationService = {
            ensureAuthentication: function (app, deferred, requestConfig) {
                // Ignore the following http requests: login/is-authenticated, login/redirect, and calls for html views/partials/templates
                if (requestConfig.url && (
                    requestConfig.url.indexOf(app.isAuthedUrl) > -1 || 
                    requestConfig.url.indexOf(app.loginRedirectUrl) > -1 ||
                    /\.html$/.test(requestConfig.url) )) {
                    deferred.resolve(requestConfig);
                    return;
                }

                //check if we're authenticated against the app
                app.isAuthed().then(function (response) {
                    if (JSON.parse(response.data.toString().toLowerCase())) { // Already authenticated :)
                        deferred.resolve(requestConfig);
                    } else { // Not authenticated :(
                        AuthenticationService.authenticate(app);
                    }
                }).catch(function () {
                    //if the request to check authentication failed
                    if (!unreachableFlag[app.url]) { //TODO: unreachableFlag is never reset. Should we find a way to reset this after some reasonable amount of time, or based on some other criteria?
                        unreachableFlag[app.url] = true;
                        $modal({title: 'Error', content: 'Failed to reach a required service which may be experiencing an outage. Application stability may suffer.', show: true, placement: 'center'});
                    }

                    deferred.reject("Server unreachable");
                });
            },

            //figure out the correct url to load, and redirect the page there
            authenticate: function (app) {
                if (authenticating) return;

                authenticating = true;
                
                if (app.preemptiveAuthentication) {
                    $http.get(app.loginRedirectUrl + $window.encodeURIComponent((app.destinationUrl) ? app.destinationUrl : $window.location.href)).error(function (response) {
                        $window.location.href = response.loginUrl;
                    });
                } else {
                    //TODO: should we have a beforeRedirect hook here, to allow apps to save data, etc. before redirecting?
                    $window.location.href = app.authAndRedirectUrl + $window.encodeURIComponent((app.destinationUrl) ? app.destinationUrl : $window.location.href);
                }
            },

            findMatchingApp: function(checkUrl) {
                if (typeof checkUrl !== "string" || isExcludedUrl(checkUrl)) {
                    return null;
                }

                // if this a relative url, and we allow that, then convert it to an absolute url and check again
                if (allowRelativeUrls && !isAbsoluteUrl(checkUrl)) {
                    //prepend the url with the current origin (this is polyfilled above). If the url doesn't start with a forwardslash, insert one
                    var absoluteUrl = $window.location.origin + (/^\//.test(checkUrl) ? '/' : '') + checkUrl;
                    
                    return AuthenticationService.findMatchingApp(absoluteUrl);
                }

                // Check baseUrl
                if (checkUrl.indexOf(Urls.baseUrl) > -1) {
                    return Urls.base;
                }

                // Check all appUrls
                var result = null;
                angular.forEach(Urls.allOtherApps, function (otherApp) {
                    if (checkUrl.indexOf(otherApp.url) > -1) {
                        result = otherApp;
                    }
                });
                return result;
            },

            login: function(app) {
                var deferred = $q.defer();

                AuthenticationService.ensureAuthentication(app, deferred, "");

                return deferred.promise;
            }
        };

        return AuthenticationService;
    }];
});




// Source: app/scripts/urls-service.js

angular.module("luAuth").provider("Urls", function () {
    var appSettingsDefaults = {
        url: "",
        loginRedirectUrl: "/login/redirect?url=", //when appended with the baseUrl, must return (or redirect to another endpoint which returns) a 401 error with an object in the response containing a loginUrl property which can be opened for the user to enter their credentials
        isAuthedUrl: "/login/is-authenticated", //url or custom function to determine if the user is already authenticated
        isAuthed: null, //function which calls isAuthedUrl (default) or a custom function to determine if user is already authenticated
        preemptiveAuthentication: true, //when true, lu-auth will call isAuthenticated (and prompting the user to authenticate if necessary) prior to requests being sent. If false, lu-auth will not ensure authentication prior to the requests being sent and instead will prompt the user to authenticate if a 401 response is received
        authAndRedirectUrl: null, //when available, this url will be loaded by the iframe to begin authentication (with the destinationUrl appended to it). This is intended for use with the /login/auth-and-redirect endpoint in webbase.
        destinationUrl: null //when available, the login-iframe will close when lu-auth detects that it has loaded this url. Note that the url must be on the same domain as your page for lu-auth to be able to see that it is loaded in the iframe. If authAndRedirectUrl is provided, this will default to the current url when authentication occurs.
        //TODO: add support for a logout url
    };

    //internal data to be configured
    var baseApp;    //this contains the configuration for the base application
    var otherApps = [];  //this contains configurations for any additional applications
    var excludedUrls = [];

    //this manually instantiates the $log service on the ng module so it can be used in this provider
    var $log = angular.injector(["ng"]).get("$log");

    //this will be dependency injected during the run phase and saved here
    var http;

    function validateAndCoerce(appSettings) {
        //TODO: provide better documentation for what constitutes a valid appSettings object

        if (appSettings == null || (typeof appSettings != "string" && typeof appSettings != "object") || (typeof appSettings == "object" && appSettings.url == null)) {
            //if appSettings is null, or it is neither a string nor an object, or if it is an object but the url property is missing:
            throw new Error("Invalid app settings: " + JSON.stringify(appSettings));
        }

        var response = angular.extend({}, appSettingsDefaults, (typeof appSettings == "string") ? {url: appSettings} : appSettings);

        //if loginRedirectUrl or isAuthedUrl start with one (and only one) forwardslash, they will be prefixed with the url
        if (typeof response.loginRedirectUrl == "string" && /^\/[^\/]/.test(response.loginRedirectUrl)) {
            response.loginRedirectUrl = response.url + response.loginRedirectUrl;
        }

        if (typeof response.isAuthedUrl == "string" && /^\/[^\/]/.test(response.isAuthedUrl)) {
            response.isAuthedUrl = response.url + response.isAuthedUrl;
        }

        if (typeof response.isAuthed !== "function") {
            response.isAuthed = function() {
                return http.get(response.isAuthedUrl);
            };
        }

        if (typeof response.authAndRedirectUrl == "string" && /^\/[^\/]/.test(response.authAndRedirectUrl)) {
            response.authAndRedirectUrl = response.url + response.authAndRedirectUrl;
        }

        if (typeof response.authAndRedirectUrl == "string" && typeof response.destinationUrl == "string") {
            response.authAndRedirectUrl += response.destinationUrl;
        }

        return response;
    }

    angular.extend(this, {
        //functions available during application config stage
        registerBaseApp: function(appSettings) {
            try {
                baseApp = validateAndCoerce(appSettings);
            } catch(e) {
                return false;
            }

            return true;
        },
        registerAdditionalApp: function(name, appSettings) {
            try {
                otherApps[name] = validateAndCoerce(appSettings);
            } catch(e) {
                $log.error(e.message);
                return false;
            }

            return true;
        },
        registerExcludedUrl: function(excludedUrl) {
            try {
                excludedUrls.push(excludedUrl);
            } catch(e) {
                throw "Failed to add excludedUrl " + excludedUrl + "; Error:" + e;
            }
        },
        validateAndCoerce: validateAndCoerce //this is here as a convenience for testing
    });

    this.$get = ["$log", "$http", function($log, $http) {
        //save a reference to the $http service to be used by the functions defined during the config phase
        http = $http;

        //verify the base app is registered
        if (!baseApp) {
            $log.error("No base app is registered!");
        }

        //available during application run stage
        var api = {
            get base() {
                return baseApp;
            },
            get baseUrl() {
                if (baseApp) {
                    return baseApp.url;
                } else {
                    return null;
                }
            },
            get excludedUrls() {
                return excludedUrls;
            },
            allOtherApps: {},
            allOtherAppUrls: {}
        };

        for (var otherAppName in otherApps) {
            if (otherApps.hasOwnProperty(otherAppName)) {
                (function(otherAppName) {
                    /*
                    N.B.: Wrapping this in an IIFE *somehow* makes it work.
                    If it isn't wrapped, then the properties created by all iterations
                    of the outer loop will match the values of the final iteration.

                    Don't ask me, I just work here.
                    */



                    //define a property on the api for each registered application that returns its configuration
                    Object.defineProperty(api.allOtherApps, otherAppName, {
                        get: function() {return otherApps[otherAppName];},
                        enumerable: true //this is enumerable so that allOtherApps can be iterated over
                    });

                    //define a property on the api for each registered application that returns its url
                    Object.defineProperty(api.allOtherAppUrls, otherAppName + "Url", {
                        get: function() {return otherApps[otherAppName].url;},
                        enumerable: true //this is enumerable so that allOtherAppUrls can be iterated over
                    });
                })(otherAppName);
            }
        }

        return api;
    }];
});
